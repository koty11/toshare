/**
 * CsvReportGenerator.js
 *
 * @description Service to generate Csv reports
 */

module.exports = {

    report: function (rptName, rows, columns, res) {

        var result = '';
        _.times(columns.length, function (i) {
            result += '"' + columns[i].name + '",';
        });

        result = result.slice(0, result.length -1);

        // Create a document
        _.times(rows.length, function (i) {
            var line = '';
            _.times(rows[i].length, function (j) {
                line += '"' + rows[i][j] + '",';
            });
            result += '\r\n' + line.slice(0, line.length -1);
        });
        var date = new Date();
        date = (('0' + date.getMonth()).slice(-2)) + (('0' + date.getDate()).slice(-2))
             + date.getFullYear()+ '_' +(('0' + date.getHours()).slice(-2)) + (('0' + date.getMinutes()).slice(-2));
        res.attachment(rptName.replace(/ /g,'') + date + '.csv');
        res.send(result);
    }
};