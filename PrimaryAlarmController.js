/**
 * PrimaryAlarmController
 *
 * @description :: Server-side logic for managing Primaryalarms
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');
module.exports = {
    create: function (req, res) {

        var Model = actionUtil.parseModel(req);

        // Create data object (monolithic combination of all parameters)
        // Omit the blacklisted params (like JSONP callback param, etc.)
        var data = actionUtil.parseValues(req);


        // Create new instance of model using data from params
        Model.create(data).exec(function created(err, newInstance) {

            // Differentiate between waterline-originated validation errors
            // and serious underlying issues. Respond with badRequest if a
            // validation error is encountered, w/ validation info.
            if (err) return res.negotiate(err);

            // If we have the pubsub hook, use the model class's publish method
            // to notify all subscribers about the created item
            if (req._sails.hooks.pubsub) {
                if (req.isSocket) {
                    Model.subscribe(req, newInstance);
                    Model.introduce(newInstance);
                }
                // Make sure data is JSON-serializable before publishing
                var publishData = _.isArray(newInstance) ?
                    _.map(newInstance, function (instance) {
                        return instance.toJSON();
                    }) :
                    newInstance.toJSON();
                Model.publishCreate(publishData, !req.options.mirror && req);
            }

            var instances = _.isArray(newInstance) ? newInstance : [newInstance];
            // Getting the array of inserted ids
            var idArray = _.map(instances, function (instance) {
                return instance.alarmId;
            });

            //Finding in the database the values that exist and update them
            var updateIdArray = new Array();
            PrimaryAlarmSnapshot.find()
                .where({id: idArray})
                .exec(function (err, response) {
                    updateIdArray = _.map(response, function (instance) {
                        return instance.id;
                    });
                    var updateArray = _.filter (instances, function(ins) {
                        return updateIdArray.indexOf(ins.alarmId) != -1;
                    });
                    updateArray =  _.map(updateArray, function (instance) {
                        if(updateIdArray.indexOf(instance.alarmId) != -1)
                        {
                            var obj = new PrimaryAlarmSnapshot._model();
                            obj.id = instance.alarmId;
                            obj.primaryId = instance.primaryId;
                            obj.type = instance.type;
                            obj.event = instance.event;
                            obj.severity = instance.severity;
                            obj.value = instance.value;
                            obj.occurrences = instance.occurrences;
                            return obj;
                        }
                    });
                    var insertArray = _.filter (instances, function(ins) {
                        return updateIdArray.indexOf(ins.alarmId) == -1;
                    });
                    //Values to insert
                    insertArray = _.map(insertArray, function (instance) {
                        if(updateIdArray.indexOf(instance.alarmId) == -1)
                        {
                            var obj = new PrimaryAlarmSnapshot._model();
                            obj.id = instance.alarmId;
                            obj.primaryId = instance.primaryId;
                            obj.type = instance.type;
                            obj.event = instance.event;
                            obj.severity = instance.severity;
                            obj.value = instance.value;
                            obj.occurrences = instance.occurrences;
                            return obj;
                        }
                    });
                    // console.log("--Values to update--");
                    // console.log(updateArray);
                    // console.log("--Values to insert--");
                    // console.log(insertArray);
                    if(insertArray != null && insertArray.length > 0) {
                        PrimaryAlarmSnapshot.create(insertArray).exec(function createCB(err, created) {
                            //console.log(created);
                        });
                    }

                    if(updateArray != null && updateArray.length > 0) {
                        updateArray.forEach(function(upd){
                            var sql = "update primaryalarmsnapshot set primaryId=" + upd.primaryId +
                                ", type=" + upd.type +
                                ", event=" + upd.event +
                                ", severity=" + upd.severity +
                                ", value=" + upd.value +
                                ", occurrences=" + upd.occurrences +
                                ", updatedAt=NOW() " +
                                "where id=" + upd.id;
                            PrimaryAlarmSnapshot.query(sql, function(err, results) {
                                if (err) return res.serverError(err);
                                return res.ok(results.rows);
                            });
                        });

                    }
                });

            NotificationService.sendLiveAlarmNotification(newInstance, true);
            // Send JSONP-friendly response if it's supported
            res.created(newInstance);
        });
    },
    subscribe: function (req, res) {
        //console.log('Subscribed to primary alarms');
        PrimaryAlarm.watch(req);
    }
};