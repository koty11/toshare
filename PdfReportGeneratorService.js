/**
 * PdfReportGenerator.js
 *
 * @description Service to generate Pdf Reports
 */

var PDFDocument = require('pdfkit');
module.exports = {

    tableReport: function (rptName, rows, columns, res) {
        // Create a document
        var doc = new PDFDocument({margin:25});

        doc.info['Title'] = rptName;
        doc.info['Author'] = 'System LLC';

        doc.pipe(res);


        PdfReportGenerator.getHeader(doc);
        var globalYpos = doc.y + 45;
        var yPos = globalYpos;
        var count = 1;
        var startAt = 25;
        _.times(rows.length, function (i) {
            startAt = 25;
            _.times(rows[i].length, function (j) {

                doc.fontSize(10)
                    .fill('black')
                    .text(rows[i][j], startAt + 3, yPos + 6);
                startAt += columns[j].width;
            });

            count += 1;
            yPos = yPos + 20;
            doc.moveTo(25, yPos)
                .lineTo(580, yPos)
                .fill('gray')
                .stroke();
            if(count == 32 ) {
                count = 1;
                PdfReportGenerator.getTable(640, columns, doc);
                doc.addPage();
                PdfReportGenerator.getHeader(doc, columns);
                yPos = globalYpos;
            }
        });

        PdfReportGenerator.getTable(yPos - globalYpos + 20, columns, doc);
        doc.end();
    },

    getTable: function (y, columns, doc) {

        doc.roundedRect(25, 105, 555, y, 5)
            .strokeColor('gray')
            .stroke();
            var startAt = 25;
            _.times(columns.length, function (i) {
                var val = columns[i];
                doc.fontSize(12)
                    .fill('black')
                    .text(val.name, startAt + 3, 110);
                if (columns.length - 1 != i) {
                    doc.moveTo(startAt + val.width, 105)
                        .lineTo(startAt + val.width, y + 105)
                        .fill('gray')
                        .stroke();
                    startAt += val.width;
                }
            });
    },

    getHeader: function (doc) {

        doc.roundedRect(150, 35, 280, 50, 5)
            .fill('gray');

        var title = doc.info['Title'];
        var factor = 300 - (title.length * 7);

        doc.fontSize(25)
            .fill('black')
            .text(title, factor, 50);

        doc.rect(25, 105, 555, 20)
            .fill('lightgray');

        doc.lineCap('butt')
            .moveTo(25, 125)
            .lineTo(580, 125)
            .stroke();

        doc.image('assets/img/red_logo.png', 15, 30, {fit: [100, 100]});
        doc.image('assets/img/mdx_logo.png', 470, 50, {fit: [100, 100]});
    }
};