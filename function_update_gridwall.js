//Every 10 Minute
schedule.scheduleJob('*/10 * * * *', function () {
    request('http://api.openweathermap.org/data/2.5/weather?zip=33137,us&APPID='+APPID, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // console.log(body);
            var bod = JSON.parse(body);
            if(bod.main){
                var outsideTemp = kToF(bod.main.temp);
                socket['/sensors'].emit('weather_update', {temp: outsideTemp, hum: bod.main.humidity, city: 'mia'});
            }
        }
    });
    request('http://api.openweathermap.org/data/2.5/weather?zip=5000,ar&APPID='+APPID, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // console.log(body);
            var bod = JSON.parse(body);
            if(bod.main) {
                var outsideTemp = kToF(bod.main.temp);
                socket['/sensors'].emit('weather_update', {temp: outsideTemp, hum: bod.main.humidity, city: 'cba'});
            }
        }
    });
    request('https://www.mytaglist.com/ethLogShared.asmx/GetLatestTemperatureRawDataByUUID?uuid=54525d7d-cc5e-434c-affc-03c728d96c8d', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            socket['/sensors'].emit('sensors', {content: body, sensor: "mia_office"});
        }
    });
    request('https://www.mytaglist.com/ethLogShared.asmx/GetLatestTemperatureRawDataByUUID?uuid=0646e5b4-fe6d-4b8d-bed9-1f997a065545', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            socket['/sensors'].emit('sensors', {content: body, sensor: "mia_server"});
        }
    });
});
//Every Hour
schedule.scheduleJob('* * * *', function () {
    var fb = new fbgraph.Facebook(SECRET, 'v2.2');
    fb.graph('/xxxxx?fields=likes&access_token='+ACCESS_TOKEN, function (err, me) {
        socket['/sensors'].emit('fb_update', {likes: me.likes});
    });
});