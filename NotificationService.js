/**
 * NotificationService.js
 *
 * @description Service to send notifications if configured
 */

function recipients(commType, commMethod, callback){
    User.query('CALL GetRecipients("'+ commType +'", "'+ commMethod +'")', function (err, result) {
        if (err) {
            res.serverError(err);
        } else {
            callback(result);
        }
    });
}

module.exports = {
    sendLiveAlarmNotification: function(alarm, isDevice){
        function onRecipientsListReady(recipientsList) {
            if(recipientsList[0].length <= 0)
                return;
            recipientsList = _.map(recipientsList[0], function (instance) {
                return instance.email;
            });
            //console.log(recipientsList);
            alarm = alarm.toDetailedJSON();
            sails.hooks.email.send(
                "testEmail",
                {
                    alarm: alarm,
                    isDevice: isDevice
                },
                {
                    bcc: recipientsList,
                    from: "testEmail<testEmail@testEmail.com>",
                    subject: alarm.valueStr
                },
                function(err) {if(err) console.log(err);}
            );
        }
        setTimeout( function () { recipients( 'Live Alarms', 'Email', onRecipientsListReady); }, 0);
    }
};