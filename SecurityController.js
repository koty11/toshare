/**
 * SecurityController
 *
 * @description :: Server-side logic for managing countries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var pager = require('sails-pager');
module.exports = {
    user: function(req, res) {
        var perPage = req.query.per_page == undefined ? 25 : req.query.per_page;
        var currentPage = req.query.page == undefined ? 1 : req.query.page;
        var search = req.query.search == undefined ? '' : req.query.search;
        var conditions = search == '' ? {} : {
            or:[
                {firstName: {
                    'like': '%' + search + '%'
                }},
                {lastName: {
                    'like': '%' + search + '%'
                }},
                {email: {
                    'like': '%' + search + '%'
                }}
            ]
        };

        pager.paginate(User, conditions, currentPage, perPage, ['userType'], 'createdAt DESC').then(function(records){
            _.times(records.data.length,function (i) {
                var value = records.data[i];
                delete value.password;
                if(value.workPhone == null)
                    value.workPhone = '';
                if(value.cellPhone == null)
                    value.cellPhone = '';
                value.fullName = value.firstName + ' ' +
                    (value.middleInitials == null || value.middleInitials == '' ? '' : (value.middleInitials  + ' ')) +
                    value.lastName;
            });
            records.meta.search = search;
            //console.log(records);
            res.view({
                 users: records,
                 fullControl: req.session.fullControl
            });
        }).catch(function(err) {
            //console.log(err);
            return next(err);
        });


        //Using a Callback
        // pager.paginate(User, conditions, currentPage, perPage, [{name: 'UserType', query: {isArchived: false}}], 'createdAt DESC', function(err, records){
        //     if(err){
        //         console.log(err);
        //     }
        //     console.log(records);
        // });
    },
    usertype: function(req, res) {
        var perPage = req.query.per_page == undefined ? 25 : req.query.per_page;
        var currentPage = req.query.page == undefined ? 1 : req.query.page;
        var search = req.query.search == undefined ? '' : req.query.search;
        var conditions = search == '' ? {} : {
            or:[
                {name: {
                    'like': '%' + search + '%'
                }},
                {description: {
                    'like': '%' + search + '%'
                }}
            ]
        };

        pager.paginate(UserType, conditions, currentPage, perPage, [], 'createdAt DESC').then(function(records){
            records.meta.search = search;
            //console.log(records);
            res.view({
                usertypes: records,
                fullControl: req.session.fullControl
            });
        }).catch(function(err) {
            //console.log(err);
            return next(err);
        });
    },
    useredit: function(req, res) {
        UserType.find().exec(function afterUserType(err, results) {
            if(req.params['id'] != null) {
                User.findOne(req.params['id']).populate('userType').exec(function afterUser(err, user){
                    if(err)
                        res.notFound();
                    else {
                        User.query('CALL GetUserCommunicationPreferences(' + user.id + ')', function (err, result) {
                            if (err) {
                                console.log(err);
                                result = null;
                            }
                                delete user.password;
                                res.view({
                                    user: user,
                                    usertypes: results,
                                    subscriptions: result == null ? [] : result[0],
                                    fullControl: req.session.fullControl || req.session.userId == parseInt(req.params['id']),
                                    adminAccess: req.session.fullControl
                                });
                        });
                    }
                });
            }
            else {
                res.view({
                    user: {
                        id: 0,
                        firstName: '',
                        lastName: '',
                        middleInitials: '',
                        workPhone: '',
                        cellPhone: '',
                        userType: {id: 1},
                        email: '',
                        isActive: true
                    },
                    usertypes: results,
                    subscription:[],
                    fullControl: req.session.fullControl,
                    adminAccess: req.session.fullControl
                });
            }
        });
    },
    accesstype:function(req, res) {
        UserType.find().exec(function (err, results) {
            var id = req.params['id'] == null ? results[0].id : req.params['id'];
            AccessRuleCategory.find().exec( function (err, categories) {
                AccessRuleByType.find({userType: id}).exec(function (err, rules) {
                    AccessRule.find().exec(function (err, allrules) {
                        _.each(allrules, function(rule){
                            var result = _.find(rules, function(value) {
                                return value.rule == rule.id;
                            });
                            rule.typeRead = (result === undefined || result == null) ? null : result.read;
                            rule.typeWrite = (result === undefined || result == null) ? null : result.write;
                            rule.typeRuleId = (result === undefined || result == null) ? 0 : result.id;
                        });
                        res.view({
                            types: results,
                            rules: allrules,
                            categories: categories,
                            id: id,
                            activeTab: 1,
                            fullControl: req.session.fullControl
                        });
                    });
                });
            });
        });
    },
    accessuser:function(req, res) {
        User.find().populate('userType').exec(function (err, results) {
            var id = req.params['id'] == null ? results[0].id : req.params['id'];
            AccessRuleCategory.find().exec( function (err, categories) {
             AccessRuleByUser.find({user: id}).exec(function (err, rules) {
                 var usr = _.find(results, function(uss) { return uss.id == id});
                 AccessRuleByType.find({userType: usr.userType.id}).exec(function (err, trules) {
                    AccessRule.find().exec(function (err, allrules) {
                    _.each(allrules, function(rule){
                        var result = _.find(rules, function(value) {
                            return value.rule == rule.id;
                        });
                        rule.userRead = (result === undefined || result == null) ? null : result.read;
                        rule.userWrite = (result === undefined || result == null) ? null : result.write;
                        rule.userRuleId = (result === undefined || result == null) ? 0 : result.id;
                        var typeRule = _.find(trules, function(trule) { return trule.rule == rule.id});
                        //console.log(typeRule);
                        rule.typeRead = (typeRule === undefined || typeRule == null) ? null : typeRule.read;
                        rule.typeWrite = (typeRule === undefined || typeRule == null) ? null : typeRule.write;
                    });
                    res.view({
                        users: results,
                        rules: allrules,
                        categories: categories,
                        id: id,
                        activeTab: 1,
                        fullControl: req.session.fullControl
                    });
                    });
                 });
            });
            });
        });
    },
};