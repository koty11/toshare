app.post('/users/:id', function(req, res){
    if(req.params.id != "all"){

        console.log("start streaming...");

        //update audience status
        var update_aud = data_con.update_aud(req.params.id,1,req,function(update_aud){
            //console.log(update_aud);
        });

        var list_aud = show_data.list_aud(req.params.id,function(list_aud){
            var location = list_aud[0];
            var latitude = location.location_lat;
            var longitude = location.location_long;

            // 6378000 Size of the Earth (in meters)
            var longitudeD = (Math.asin(parseFloat(0.0120*100000*location.radius) / (6378000 * Math.cos(Math.PI*latitude/180))))*180/Math.PI;
            var latitudeD = (Math.asin(parseFloat(0.0120*100000*location.radius) / parseFloat(6378000)))*180/Math.PI;
            var latitudeMax  = parseFloat(latitude)+parseFloat(latitudeD);
            var latitudeMin  = parseFloat(latitude)-parseFloat(latitudeD);
            var longitudeMax = parseFloat(longitude)+parseFloat(longitudeD);
            var longitudeMin = parseFloat(longitude)-parseFloat(longitudeD);
            var global_user_id = [];
            client.stream('statuses/filter', {'locations':''+longitudeMin.toFixed(6)+','+latitudeMin.toFixed(6)+','+longitudeMax.toFixed(6)+','+latitudeMax.toFixed(6)+''}, function(stream) {
                
                global_stream = stream;
                stream.on('data', function (data) {

    			   if(global_user_id.indexOf(data.user.id) < 0 ){
            	       var temp_save_counter = data_con.save_counter_x_aud(req.params.id,data,req,res);
    	               var hispanic_return = data_con.is_hispanic(req.params.id,data.user,req,res,function(hispanic_return){

                	       console.log("hispanic_return: ",hispanic_return.dist);
                            if((hispanic_return.dist >= 0.91 && hispanic_return.dist <= 1.1) || (hispanic_return.dist > 35 && hispanic_return.dist > 75 )){
      		                    var temp = data_con.save_user_x_aud(req.params.id,hispanic_return,req,res);
       	                            global_user_id.push(data.user.id);
                                }

                            else if((hispanic_return.dist > 30 && hispanic_return.dist < 35) || (hispanic_return.dist > 0.88 && hispanic_return.dist < 0.90) ){
                                var temp = data_con.temp_save_user_x_aud(req.params.id,hispanic_return,req,res,function(temp){
                    	           if(socket['/users/'+req.params.id].sockets[0]){
                    	               global_user_id.push(data.user.id);
                	                   socket['/users/'+req.params.id].emit('message', { message: data.user });
                	               }
            	                });

        	                }
                            else{
                                global_user_id.push(data.user.id);	
                            }
                                

                        });
    						
    				}
    						
    						
                    /*count save users*/
                    var count_user = data_con.count_users_x_aud(req.params.id,req,res,function(count_user){

                        if(socket['/users/'+req.params.id].sockets[0]){
                            socket['/users/'+req.params.id].emit('count_user', { count_user: count_user });
                        }
                    });
                    
                    /*count temp save users*/
                    var temp_count_user = data_con.temp_count_users_x_aud(req.params.id,req,res,function(temp_count_user){
                        if(socket['/users/'+req.params.id].sockets[0]){
                            socket['/users/'+req.params.id].emit('temp_count_user', { temp_count_user: temp_count_user });
                        }
                    });
                    
                    /*save counter*/
                    var counter_x_aud = data_con.counter_x_aud(req.params.id,req,res,function(counter_x_aud){
                        if(socket['/users/'+req.params.id].sockets[0]){
                            socket['/users/'+req.params.id].emit('counter_x_aud', { counter_x_aud_tweet: counter_x_aud[0].tweet_count, counter_x_aud_user:counter_x_aud[0].user});
                        }
                    });
                    
                    /***************/

                });
                
                stream.on('destroy', function (response) {
                    console.log("Destroy");
                });

            });

        });

    }

});