/**
 * AccessControlService.js
 *
 * @description Service to generate validate incoming requests access
 */

module.exports = {
    userAccess: function (path, userId, callback){
        var slash = path.lastIndexOf("/");
        if(slash == path.length - 1 || !isNaN(path.substring(slash + 1)))
            path = path.substring(0, slash);
        AccessRule.query('CALL GetPathAccess("'+ path +'", ' + userId + ');', function onData(err, rule) {
            if(err || rule[0].length == 0) {
                return callback({read: false, write: false, isActive: false});
            }
            return callback({
                    read: rule[0][0].read == null ? false : rule[0][0].read,
                    write: rule[0][0].write == null ? false : rule[0][0].write,
                    isActive: rule[0][0].isActive
                });
        });
    }
};