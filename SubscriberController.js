/**
 * SubscriberController
 *
 * @description :: Server-side logic for managing subscribers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var PythonShell = require('python-shell');

module.exports = {

	scriptCalls : function(req, res) {

		if (!req.isSocket) {
			//console.log('subscriber a');
			return res.badRequest();
		}

		var roomName = 'analytics_'+req.param('userId');
		sails.sockets.join(req, roomName, function(err) {
			if (err) {
				console.log('subscriber b');
				return res.serverError(err);
			}
			console.log('subscriber c');
			return res.json({
				message: 'Subscribed to a fun room called '+roomName+'!'
			});
		});

	},
	stopScript : function(req, res){
		


    	var options = {
			scriptPath: sails.config.script_path
		};

		try {
			PythonShell.run('stop_script.py', options, function (err, results) {

				console.log('results: %j', results);
        		console.log('err: %j', err);

			});
		}
		catch(err) {
		    console.log('err', results);
		}
	},
	runHypercube : function(req, res){

		//console.log('\"'+req.param('hypercube')+'\"')

		
		var options = {
			mode : 'text',
			scriptPath: sails.config.script_path,
			args: [req.param('hypercube'), req.param('userId')]
		};

		try {
			PythonShell.run('generate_hipercube.py', options, function (err, results) {

				console.log('results: %j', results);

			});
		}
		catch(err) {
		    console.log('err', results);
		}
	}

};
