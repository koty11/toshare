function subscribeReadValue(callback) {
    subscribe('readvalue', callback);
}
function subscribePrimaryAlarm(callback) {
    subscribe('primaryalarm', callback);
}

function subscribeSecondaryAlarm(callback) {
    subscribe('secondaryalarm', callback);
}

function subscribe(route, callback) {
    socketCall('get','/'+ route +'/subscribe', '');
    io.socket.on(route, function onServerSentEvent(msg) {
        if (typeof callback != 'undefined')
            callback(msg.data);
        else
            console.log("callback is undefined");
    });
}