/**
 * LampController
 *
 * @description :: Server-side logic for managing lamps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var pager = require('sails-pager');
module.exports = {
    search: function (req, res) {
        var params = req.allParams();
        delete params.id;

        Lamp.find({
            serialNumber: {
                'like': '%' + params.name + '%'
            }
        }, {
            limit: params.limit || 10,
            skip: params.skip || 0
        }).exec(function (err, results) {
            return res.ok(results);
        });
    },

    details: function (req, res) {

        var perPage = req.query.per_page == undefined ? 25 : req.query.per_page;
        var currentPage = req.query.page == undefined ? 1 : req.query.page;
        var search = (req.query.search == undefined || req.query.search == '') ? null : JSON.parse(req.query.search);
        var conditions = {};
        RoadInformation.find().exec(function (err, roads) {
            LoadCenter.find().exec(function (err, lcenters) {
                Primary.find().exec(function (err, primaries) {
                    Secondary.find().exec(function (err, secondaries) {
                        Vendor.find().exec(function (err, vendors) {
                            _.times(lcenters.length, function (i) {
                                var value = lcenters[i];
                                value.roadInfo = _.find(roads, function (road) {
                                    return road.id == value.roadInfo;
                                });
                            });
                            _.times(primaries.length, function (i) {
                                var value = primaries[i];
                                value.loadCenter = _.find(lcenters, function (load) {
                                    return load.id == value.loadCenter;
                                });
                            });
                            _.times(secondaries.length, function (i) {
                                var value = secondaries[i];
                                value.primary = _.find(primaries, function (prim) {
                                    return prim.id == value.primary;
                                });
                            });
                            if (req.query.search != undefined) {
                                if (search != null) {
                                    var selectedSecondaries = secondaries;
                                    if (search.road != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.primary.loadCenter.roadInfo.id == search.road;
                                        });
                                    }
                                    if (search.load != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.primary.loadCenter.id == search.load;
                                        });
                                    }
                                    if (search.primary != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.primary.id == search.primary;
                                        });
                                    }
                                    if (search.secondary != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.id == search.secondary;
                                        });
                                    }
                                    if (search.vendor != null) {
                                        conditions.vendor = search.vendor;
                                    }
                                    if (search.status != null) {
                                        conditions.isActive = search.status
                                    }
                                    if (selectedSecondaries.length == 0)
                                        conditions.secondary = 0;
                                    else if (selectedSecondaries.length != secondaries.length)
                                        conditions.secondary = _.map(selectedSecondaries, function (record) {
                                            return record.id;
                                        });
                                }
                                pager.paginate(Lamp, conditions, currentPage, perPage, ['position'], 'createdAt DESC').then(function (records) {
                                    _.times(records.data.length, function (i) {
                                        var value = records.data[i];
                                        value.secondary = _.find(secondaries, function (sec) {
                                            return sec.id == value.secondary;
                                        });
                                        if (value.vendor > 0)
                                            value.vendor = _.find(vendors, function (vend) {
                                                return vend.id == value.vendor;
                                            });
                                    });
                                    records.meta.search = search;
                                    res.view('reports/management/lamp', {
                                        lamps: records,
                                        vendors: vendors,
                                        secondaries: secondaries,
                                        roads: roads,
                                        loads: lcenters,
                                        primaries: primaries,
                                        conditions: search ? search : {}
                                    });
                                }).catch(function (err) {
                                    //console.log(err);
                                    return next(err);
                                });
                            }
                            else {
                                res.view('reports/management/lamp', {
                                    lamps: {data: [], meta: {page: 1, pageCount: 1, perPage: perPage}},
                                    vendors: vendors,
                                    secondaries: secondaries,
                                    roads: roads,
                                    loads: lcenters,
                                    primaries: primaries,
                                    conditions: search ? search : {}
                                });
                            }
                        });
                    });
                });
            });
        });
    },
    ////////////////////////////
    ///// PDF GENERATION //////
    ///////////////////////////

    detailsreport: function (req, res) {
        var format = req.query.format == undefined ? 'PDF' : req.query.format;

        var columns = [
            {name: 'Id', width: 30},
            {name: 'Secondary', width: 70},
            {name: 'Primary', width: 55},
            {name: 'Load Center', width: 75},
            {name: 'Road Info', width: 65},
            {name: 'Latitude', width: 60},
            {name: 'Longitude', width: 60},
            {name: 'Position', width: 50},
            {name: 'Vendor', width: 60}
        ];
        var search = (req.query.search == undefined || req.query.search == '') ? null : JSON.parse(req.query.search);
        var conditions = {};
        RoadInformation.find().exec(function (err, roads) {
            LoadCenter.find().exec(function (err, lcenters) {
                Primary.find().exec(function (err, primaries) {
                    Secondary.find().exec(function (err, secondaries) {
                        Vendor.find().exec(function (err, vendors) {
                            _.times(lcenters.length, function (i) {
                                var value = lcenters[i];
                                value.roadInfo = _.find(roads, function (road) {
                                    return road.id == value.roadInfo;
                                });
                            });
                            _.times(primaries.length, function (i) {
                                var value = primaries[i];
                                value.loadCenter = _.find(lcenters, function (load) {
                                    return load.id == value.loadCenter;
                                });
                            });
                            _.times(secondaries.length, function (i) {
                                var value = secondaries[i];
                                value.primary = _.find(primaries, function (prim) {
                                    return prim.id == value.primary;
                                });
                            });
                            if (req.query.search != undefined) {
                                if (search != null) {
                                    var selectedSecondaries = secondaries;
                                    if (search.road != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.primary.loadCenter.roadInfo.id == search.road;
                                        });
                                    }
                                    if (search.load != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.primary.loadCenter.id == search.load;
                                        });
                                    }
                                    if (search.primary != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.primary.id == search.primary;
                                        });
                                    }
                                    if (search.secondary != null) {
                                        selectedSecondaries = _.filter(selectedSecondaries, function (o) {
                                            return o.id == search.secondary;
                                        });
                                    }
                                    if (search.vendor != null) {
                                        conditions.vendor = search.vendor;
                                    }
                                    if (search.status != null) {
                                        conditions.isActive = search.status
                                    }
                                    if (selectedSecondaries.length == 0)
                                        conditions.secondary = 0;
                                    else if (selectedSecondaries.length != secondaries.length)
                                        conditions.secondary = _.map(selectedSecondaries, function (record) {
                                            return record.id;
                                        });
                                }
                            }
                            Lamp.find(conditions).populate('position').exec(function (err, records) {
                                var rows = [];
                                _.times(records.length, function (i) {
                                    var value = records[i];
                                    value.secondary = _.find(secondaries, function (sec) {
                                        return sec.id == value.secondary;
                                    });
                                    if (value.vendor > 0)
                                        value.vendor = _.find(vendors, function (vend) {
                                            return vend.id == value.vendor;
                                        });

                                    rows.push([
                                        value.id,
                                        value.secondary.serialNumber,
                                        value.secondary.primary.serialNumber,
                                        value.secondary.primary.loadCenter.name,
                                        value.secondary.primary.loadCenter.roadInfo.name,
                                        value.secondary.latitude,
                                        value.secondary.longitude,
                                        value.position == null ? '' : value.position.name,
                                        value.vendor == null ? '' : value.vendor.name
                                    ]);
                                });
                                ////// I AM DONE GETTING THE DATA, LETS CREATE THE REPORT
                                if (format != 'PDF') {
                                    CsvReportGenerator.report("Lamp Details", rows, columns, res);
                                }
                                else
                                    PdfReportGenerator.tableReport("Lamp Details", rows, columns, res);
                            });
                        });
                    });
                });
            });
        });
    },
};
